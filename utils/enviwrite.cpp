#include "enviwrite.h"
#include <qgsrasterlayer.h>
#include <QFile>
#include <QTextStream>

bool enviwrite(const QString& base, QgsRasterDataProvider* rdp)
{
  size_t w = rdp->xSize();
  size_t h = rdp->ySize();
  const size_t bw = rdp->xBlockSize();
  const size_t bh = rdp->yBlockSize();
  const size_t ts = rdp->dataTypeSize(1)/8;
  std::cerr << "bwxbhxsz = " << bw << "x" << bh << "x" << ts << ".\n";

  const size_t blks_y = (h+bh-1)/bh;
  const size_t blks_x = (w+bw-1)/bw;
  
  //ToDo: fix
  w = blks_x*bw;
  h = blks_y*bh;

  QFile hdr_f(base + ".hdr");
  hdr_f.open(QIODevice::WriteOnly);
  QTextStream hdr(&hdr_f);
  
  hdr << "ENVI\nsamples = " << w << "\n";
  hdr << "lines = " <<  h << "\n";
  hdr << "bands = " << rdp->bandCount() << "\n";
  hdr << "header offset = 0\nfile type = ENVI Standard\n";
  //ToDo: assume all the bands are of the same type
  int t=0;
  switch (rdp->dataType(1))
    {
    case QgsRasterDataProvider::Byte:
      t = 1;
      break;
    case QgsRasterDataProvider::UInt16:
      t = 12;
      break;
    case QgsRasterDataProvider::Int16:
      t = 2;
      break;
    case QgsRasterDataProvider::UInt32:
      t = 13;
      break;
    case QgsRasterDataProvider::Int32:
      t = 3;
      break;
    case QgsRasterDataProvider::Float32:
      t = 4;
      break;
    case QgsRasterDataProvider::Float64:
      t = 5;
      break;
    default:
      break;
    }
  // ToDo: Little Endian
  hdr << "data type = " << t << "\ninterleave = bsq\nbyte order = 0\n";

  QFile data_f(base);  
  data_f.open(QIODevice::WriteOnly); 
  // ToDo: check dataType all the same for all bands
  QByteArray buffer(bw*bh*ts);
  for (int b = 1; b <= rdp->bandCount(); ++b)
    {
      for (int by=0; by<blks_y; ++by)
	for (int bx=0; bx<blks_x; ++bx)
	  {
	    rdp->readBlock(b, bx, by, buffer.data());
	    for (int r=0; r<bh; ++r)
	      {
		data_f.seek(((r+by*bh)*w + bx*bw)*ts);
		data_f.write(buffer.data()+r*bw*ts, bw*ts);
	      }
	  }
    }

  return true;
}

