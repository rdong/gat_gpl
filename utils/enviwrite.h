#ifndef ENVIWRITE_H
#define ENVIWRITE_H

class QgsRasterDataProvider;
class QString;
bool enviwrite(const QString& base, QgsRasterDataProvider* rdp);

#endif
