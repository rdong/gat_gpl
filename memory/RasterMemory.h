#ifndef RASTERMEMORYPROVIDER_H
#define RASTERMEMORYPROVIDER_H

#include <qgsrasterdataprovider.h>
#include "mclcppclass.h"	/* mwArray */
#include <qgsrectangle.h>
#include <qgsrasterlayer.h>

class RasterMemoryProvider: public QgsRasterDataProvider
{
 public:
  RasterMemoryProvider(){};
  QgsCoordinateReferenceSystem crs() {return QgsCoordinateReferenceSystem(); }
  bool isValid() { return true; }
  QString name() const { return "RasterMemoryProvider"; }
  QString description() const;
  void addLayers(const QStringList&, const QStringList&) {};
  QStringList supportedImageEncodings() {return QStringList();}
  QString imageEncoding() const {return "";}
  void setImageEncoding(const QString&){}
  void setImageCrs(const QString&){}
  QImage* draw(const QgsRectangle&, int, int) 
    { std::cerr << "RasterMemoryProvider::draw.\n"; return 0;}
  QString metadata() {return "";}
  QString identifyAsText(const QgsPoint&) {return "identifyAsText"; }
  QString identifyAsHtml(const QgsPoint&) {return "identifyAsHtml"; }
  QString lastErrorTitle() {return "lastErrorTitle"; }
  QString lastError() {return "lastError"; }
  double noDataValue() const { return std::numeric_limits<double>::quiet_NaN(); }


  void readBlock(int bandNo, QgsRectangle  const & viewExtent, int width, int height, void *data);
  void readBlock(int bandNo, int xBlock, int yBlock, void *data);

  QgsRectangle extent()
  {
    QgsRectangle ret;
    if (_R.IsEmpty()) return ret;
    mwArray R1 = _R;
    if (mxCELL_CLASS == _array.ClassID()) R1 = _R(1);
    return QgsRectangle(R1(3, 1), double(R1(3, 2))+double(R1(1, 2))*ySize(),
			double(R1(3, 1))+double(R1(2, 1))*xSize(), R1(3, 2));
  }
  bool setDataSource(const mwArray& array, const mwArray& R)
  {
    if (array.IsEmpty()) return false;
    mwArray array1 = _array;
    if (mxCELL_CLASS == array.ClassID()) array1 = _array(1);
    if (!array1.IsNumeric()) return false;
    mwArray dims = array1.GetDimensions();
    if (2 != dims.NumberOfElements()) return false;

    if (R.IsEmpty()) return false;
    mwArray R1 = R;
    if (mxCELL_CLASS == _array.ClassID()) R1 = R(1);
    if (!R1.IsNumeric()) return false;
    dims = R1.GetDimensions();
    if (2 != dims.NumberOfElements()) return false;
    // check size
    // if (dims(1) == (mxInt32)3 && dim2(2) == (mxInt32)2 ) return false;
    _array = array;
    _R = R;
    return true;
  }

  int srcDataType( int bandNo ) const;

  int xBlockSize() const { return xSize(); }
  int yBlockSize() const { return ySize(); }

  int xSize() const
  {
    if (_array.IsEmpty()) return 0;
    mwArray array1 = _array;
    if (mxCELL_CLASS == _array.ClassID() ) array1 = _array(1);
    mwArray dims = array1.GetDimensions();
    std::cerr << "RasterMemoryProvider::xSize() = " << dims(2) << ".\n";
    return dims(2);
  }

  int ySize() const
  {
    if (_array.IsEmpty()) return 0;
    mwArray array1 = _array;
    if (mxCELL_CLASS == _array.ClassID() ) array1 = _array(1);
    mwArray dims = array1.GetDimensions();
    std::cerr << "RasterMemoryProvider::ySize() = " << dims(1) << ".\n";
    return dims(1);
  }
  ~RasterMemoryProvider(){};

 private:
  mwArray _array;
  mwArray _R;
};

class RasterMemoryLayer: public QgsRasterLayer
{
 public:
  RasterMemoryLayer(const QString& name, const mwArray& array, const mwArray& R)
      :QgsRasterLayer(0, "qsm", name, "RasterMemoryProvider_Key")
    {
      RasterMemoryProvider* rmp = dataProvider();
      rmp->setDataSource(array, R);
      //      setNoDataValue( rmp->noDataValue() );

      // get the extent
      QgsRectangle mbr = rmp->extent();
      mLayerExtent.setXMaximum( mbr.xMaximum() );
      mLayerExtent.setXMinimum( mbr.xMinimum() );
      mLayerExtent.setYMaximum( mbr.yMaximum() );
      mLayerExtent.setYMinimum( mbr.yMinimum() );
    }

  RasterMemoryProvider* dataProvider()
    {
      QgsRasterDataProvider* rdp = QgsRasterLayer::dataProvider();
      return dynamic_cast<RasterMemoryProvider*>(rdp);
    }

  ~RasterMemoryLayer(){}
};

#endif
