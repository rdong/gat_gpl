#include "RasterMemory.h"
#include <QTextStream>
#include <iostream>

#ifdef RASTERMEMORY
QGISEXTERN RasterMemoryProvider *classFactory(const QString *uri)
{
  std::cerr << "RasterMemoryProvider classFactory(" << uri->toStdString() << ").\n";
  return new RasterMemoryProvider;
}

QGISEXTERN QString providerKey()
{
  return "RasterMemoryProvider_Key";
}

/**
 * Required description function
 */

QGISEXTERN QString description()
{
  std::cerr << "RasterMemoryProvider_desc.\n";
  return "RasterMemoryProvider_desc";
}

/**
 * Required isProvider function. Used to determine if this shared library
 * is a data provider plugin
 */
QGISEXTERN bool isProvider()
{
  return true;
}

#endif // RASTERMEMORY

QString RasterMemoryProvider::description() const
{
  //  return ::description();
  return "RasterMemoryProvider_desc";
}

/* Array is a cell array of 2D Matrix, R is 3x2 reference matrix */

int RasterMemoryProvider::srcDataType( int bandNo ) const
{
  int ret = QgsRasterDataProvider::UnknownDataType;
  if (_array.IsEmpty()) return ret;

  mxClassID cl = _array.ClassID();
  if (mxCELL_CLASS == cl) cl = _array(1).ClassID();

  switch (cl)
    {
    case mxCHAR_CLASS:
      ret = QgsRasterDataProvider::Byte;
      break;
    case mxDOUBLE_CLASS:
      ret = QgsRasterDataProvider::Float64;
      break;
    case mxSINGLE_CLASS:
      ret = QgsRasterDataProvider::Float32;
      break;
    case mxINT8_CLASS:
      ret = QgsRasterDataProvider::Byte;
      break;
    case mxUINT8_CLASS:
      ret = QgsRasterDataProvider::Byte;
      break;
    case mxINT16_CLASS:
      ret = QgsRasterDataProvider::Int16;
      break;
    case mxUINT16_CLASS:
      ret = QgsRasterDataProvider::UInt16;
      break;
    case mxINT32_CLASS:
      ret = QgsRasterDataProvider::Int32;
      break;
    case mxUINT32_CLASS:
      ret = QgsRasterDataProvider::UInt32;
      break;
    default:
      break;
    }
  return ret;
}

void RasterMemoryProvider::readBlock(int bandNo, QgsRectangle  const & viewExtent,
				     int width, int height, void *data)
{
  std::cerr << "RasterMemoryProvider::readBlock(" << bandNo << ", " << viewExtent
	    << " " << width << "x" << height << ").\n";
  //  mwArray array1 = _array(bandNo);
  mwArray array1 = _array;
  std::cerr << "RasterMemoryProvider::readBlock mxClassID  = " <<  array1.ClassID() << ".\n";
  array1.GetData((mxSingle*)data, width*height);
}

void RasterMemoryProvider::readBlock(int bandNo, int xBlock, int yBlock, void *data)
{
  std::cerr << "RasterMemoryProvider::readBlock(" << bandNo << ", "
	    << xBlock << ", " << yBlock << ").\n";
  mwArray array1 = _array;
  std::cerr << "RasterMemoryProvider::readBlock mxClassID  = " <<  array1.ClassID() << ".\n";
  array1.GetData((mxSingle*)data, xBlockSize()*yBlockSize());
}
