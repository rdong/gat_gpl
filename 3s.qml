<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="1.7.3-Wroclaw" minimumScale="0" maximumScale="1e+08" hasScaleBasedVisibilityFlag="0">
  <transparencyLevelInt>255</transparencyLevelInt>
  <rasterproperties>
    <mDrawingStyle>SingleBandPseudoColor</mDrawingStyle>
    <mColorShadingAlgorithm>PseudoColorShader</mColorShadingAlgorithm>
    <mInvertColor boolean="false"/>
    <mRedBandName>Not Set</mRedBandName>
    <mGreenBandName>Not Set</mGreenBandName>
    <mBlueBandName>Not Set</mBlueBandName>
    <mGrayBandName>Band 1</mGrayBandName>
    <mStandardDeviations>3</mStandardDeviations>
    <mUserDefinedRGBMinimumMaximum boolean="false"/>
    <mRGBMinimumMaximumEstimated boolean="true"/>
    <mUserDefinedGrayMinimumMaximum boolean="false"/>
    <mGrayMinimumMaximumEstimated boolean="true"/>
    <mContrastEnhancementAlgorithm>NoEnhancement</mContrastEnhancementAlgorithm>
    <contrastEnhancementMinMaxValues>
      <minMaxEntry>
        <min>-3.40282e+38</min>
        <max>3.40282e+38</max>
      </minMaxEntry>
    </contrastEnhancementMinMaxValues>
    <mNoDataValue mValidNoDataValue="true">nan</mNoDataValue>
    <singleValuePixelList>
      <pixelListEntry pixelValue="nan" percentTransparent="100"/>
    </singleValuePixelList>
    <threeValuePixelList>
      <pixelListEntry red="nan" blue="nan" green="nan" percentTransparent="100"/>
    </threeValuePixelList>
  </rasterproperties>
</qgis>
