			Geospatial Analytics Tools

Introduction:

	I developed a Geospatial Analytics Tools for Quantum GIS (QGIS www.qgis.org). The Geospatial Analytics Tools use platform independent QT (http://qt.nokia.com/products/) and work on Linux, MacOS and Windows.  Currently, only Linux and MacOS are supported.  Work is underway to implement some of the modules on the web server backend.  3D (stereo) visualization will still require local application.

Architecture Overview:
	
	gatLauncher is implemented as a QGIS plugin.   Upon loading, it searches through the subdirectory "plugins" in QGIS plugin directory and launches each statistical module into its own tab.  The gatLauncher gets data from active rasters and vector layers in QGIS and pass the data to the Statistical Modules.  The gatLauncher will also load the outputs from the Statistical Modules into QGIS as new (raster or vector) layers.  The gatLauncher is covered under GNU GENERAL PUBLIC LICENSE version 2 (GPL v2).

	I have implemented five(5) statistical modules

	1) QR2:	QR factoring 

	2) MinDist: Minimal Distance with eleven(11) distances implemented, "euclidean" , "seuclidean", "cityblock", "mahalanobis", "minkowski", "cosine", "correlation", "spearman", "hamming", "jaccard", "chebychev".
		   
	3) SDA: Stepwise Discriminant Analysis

	4) Cluster:  Unsupervised Classification 
	   
	   Currently, only K-Means is implemented with the following five (5) distances

           'sqEuclidean'  - Squared Euclidean distance (the default)

           'cityblock'    - Sum of absolute differences, a.k.a. L1 distance

           'cosine'       - One minus the cosine of the included angle between points (treated as vectors)

           'correlation'  - One minus the sample correlation between points (treated as sequences of values)

           'Hamming'      - Percentage of bits that differ (only suitable for binary data)

        5) Correlation:  Correlation

	   Compute Correlation among all active raster layers restricted by the optional Region of Interests (ROI) from any active polygon vector layer.

	These modules do not use any GPL code and are therefore not covered under GPL.  All five modules use the royalty free Matlab runtime. One of the design goals is that I would be able to modify and/or implement new Analytics Modules easily and quickly.  New module(s) can be loaded into a running Quantum GIS session on the fly.  More modules are expected to be written for different types of Statistical Modeling.  I'm also prototyping a stereo visualization module.

To use Geospatial Analytics Tools, install Matlab runtime and setup your environment variables LD_LIBRARY_PATH (DYLD_LIBRARY_PATH for MacOS) and XAPPLRESDIR accordingly.

To run the Geospatial Analytics Tools the first time, Select Settings/General/Plugin paths.  Add the directory containing libgatLauncher.so.  Select plugins/Manages Plugins and check gat Launcher.

To run these modules, load the raster data layers and vector training set should it be required.  Please note that only active layers are used.  Select Plugins/gatLauncher/Launch.  Five(5) tabs will show up MinDist, QR2, SDA, Culster, Correlation, Select your training area and click Apply.

Development Environment:

	Ubuntu Precise Pangolin (12.04) LTS AMD64(x86_64).
	gcc 4.6.3
	QT 4.8.1
	qgis 1.8.0 (Lisboa) from https://launchpad.net/~ubuntugis/+archive/ppa
	Matlab R2012a UNIX

	MacOS Snow Leopard x86_64
	Xcode 3.2.6
	gcc darwin 4.2.1
	QT 4.8.0
	qgis 1.8.0 (Lisboa) from http://www.kyngchaos.com/software/qgis
	Matlab R2012a UNIX

	I do not currently have a Windows development environment. I will cross compile Windows binaries from Linux instead.  I willl also need a Windows version of Matlab.

Git Repositories:

	 The Git Repository is at 

git clone 

Hardware Consideration:

	 Stereo capable computer with Nvidia Quadro card. Nvidia active stereo works out of box in recent Linux (e.g Ubuntu Precise Pangolin LTS) with Acer GD235HZ LCD Monitor and H5360 projector.  Windows 7 is supported, but software/driver configuration is very involved.  I haven't get it fully functional.

	 We also need a 3D mouse to pick points in 3D space.  SpaceNavigator (http://www.3dconnexion.com/products/spacenavigator.html) is an inexpensive USB 3D mouse and is well supported in Linux and QT.
	 
