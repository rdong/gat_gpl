#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <QObject>
#include <QList>
#include "qgisplugin.h"
#include <QDialog>
#include "ui_Launcher.h"

class QAction;
class QPluginLoader;
namespace qsm
{
  class Launcher: public QObject, public QgisPlugin
  {
    Q_OBJECT
  public:
    Launcher(QgisInterface *iface)
      : _Iface(iface), _LaunchAction(0) /*, _ILauncher(0)*/ {}
    virtual ~Launcher(){};
    void initGui();
    void unload();
    
  private:
    QgisInterface* _Iface;
    QAction* _LaunchAction;

  private slots:
  void launch();
  };

  class PluginInterface;
  class LauncherInterface;
  class Launcher_diag: public QDialog, private Ui::Dialog
  {
    Q_OBJECT
      public:
    Launcher_diag(LauncherInterface* Iface, QgisInterface* qgis);
    virtual ~Launcher_diag();
    QPlainTextEdit* log() { return logText; }

    int addTab(QWidget* page, const QString & label)
    {
      return tabWidget->addTab(page, label);
    }

    private slots:
    void resultsReady();

  private:
    LauncherInterface* _Iface;
    QgisInterface* _qgis;
    QList<QPluginLoader*> _loaders;
  };
};

#endif
