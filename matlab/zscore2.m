function Y = zscore2(X)
    X0 = cat(3, X{:});
    [h, w, d] = size(X0);
    idx_ft = all(isfinite(X0), 3);

    for l=1:d
        Y{l} = NaN(h, w);
        Y{l}(idx_ft) = zscore(X{l}(idx_ft));
    end
end
