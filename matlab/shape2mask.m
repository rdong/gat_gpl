function Mask = shape2mask(Shape, R, sz)
    Mask = zeros(sz);
    for l=1:length(Shape)
        [r, c] = xy2rc(Shape(l).X, Shape(l).Y, R);
        r=r(isfinite(r));
        c=c(isfinite(c));
        Mask = Mask | poly2mask(c, r, double(sz(1)), double(sz(2)));
    end
end
  
  
