function s = contour2shape(cs)

  s = struct([]);
  i = 1;
  while i < size(cs, 2)
    
    % Read in contour value and number of vertices
    val = cs(1, i);
    b = i+1;
    e = i+cs(2, i);

    s1.Val = round(val);
    s1.Name = num2str(round(val));
    s1.X = cs(1, b:e);
    s1.Y = cs(2, b:e);
    s1.Geometry = 'Polygon';

    if ((abs(s1.X(1) - s1.X(end)) < eps) & (abs(s1.Y(1) - s1.Y(end)) < eps))
        n = e-b+1;
        n2 = round(1:0.5:(n-0.5));
        g = 2*mod(1:(2*n-2), 2)-1;
        n1 = n2 + g;
        s1.Area = (s1.X(n2).*s1.Y(n1))*g';
        s1.Area = abs(s1.Area/2/1000000);
    else
        s1.Area = 0;
    end
    s = [s, s1];
    i = e + 1;
  end
  [N,  I] = sort([s.Val]);
  s = s(I);
end
