function enviwrite(m, fname, R)
% ENVIWRITE(m, fname, R, 'utmzone', '11', 'hemisphere', 'North')
    t = 15;
    %% mirone can't read double
    if (isa(m, 'double'))
        m = single(m);
    end
    
    varargin = [];
    utmzone='0';
    hemisphere='North'; 

    global MAP_VAR_LIST;

    if exist('MAP_VAR_LIST')
        if isfield(MAP_VAR_LIST, 'zone')
            utmzone = num2str(MAP_VAR_LIST.zone);
        end

        if isfield(MAP_VAR_LIST, 'hemisphere')
            hemisphere = MAP_VAR_LIST.hemisphere;
        end
    end
        
    for t = 1:2:length(varargin)
        eval([ varargin{t} '= ''' varargin{t+1} ''';' ]);
    end
    [h, w, d] = size(m);
    fid = fopen(fname, 'w+');
    %% BSQ?
    for l=1:d
        bsq = m(:, :, l);
        fwrite(fid, bsq', class(m));
    end
    fclose(fid);

    fid = fopen(sprintf('%s.hdr', fname), 'w+');
    fprintf(fid, 'ENVI\nsamples = %d\nlines = %d\nbands = %d\nheader offset = 0\nfile type = ENVI Standard\n', w, h, d);

    t = 0;
    if isa(m, 'uint8') 
        t = 1;
    elseif isa(m, 'int16')
        t = 2;
    elseif isa(m, 'int32')
        t = 3;
    elseif isa(m, 'single')
        t = 4;
    elseif isa(m, 'double')
        t = 5;
        %% complex
    elseif isa(m, 'uint16')
        t = 12;
    elseif isa(m, 'uint32')
        t = 13;
    elseif isa(m, 'int64')
        t = 14;
    elseif isa(m, 'uint64')
        t= 14;
    end
    fprintf(fid, 'data type = %d\ninterleave = bsq\nbyte order = 0\n', t);
    if (exist('R'))
        fprintf(fid, 'map info = {UTM, 1, 1, %f, %f, %f, %f', ...
                R(3, 1), R(3, 2), R(2, 1), -R(1, 2));
        if (utmzone ~= '0')
            fprintf(fid, '%s, %s, WGS-84', utmzone, hemisphere);
        end
        fprintf(fid, '}\n');
        fprintf(fid, 'pixel size = { %f, %f, units=meters }\n', R(2, 1), -R(1, 2));
    end
    fclose(fid);
end
