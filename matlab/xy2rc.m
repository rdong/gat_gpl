function [r, c] = xy2rc(x, y, R)
  R1 = inv(R(1:2, :))';
  r = floor(R1(1, 1)*(x-R(3, 1))+R1(1, 2)*(y-R(3, 2)))+1;
  c = floor(R1(2, 1)*(x-R(3, 1))+R1(2, 2)*(y-R(3, 2)))+1;
end
