function Y = mahalanobis(X, mask)
    X0 = cat(3, X{:});
    [h, w, d] = size(X0);
    idx_ft = all(isfinite(X0), 3);
    
    for d1=1:d
        V(:, d1) = double(X{d1}(idx_ft));
        V_X(:, d1) = double(X{d1}(mask&idx_ft));
    end
    
    [rx, cy] = size(V_X);
    [ry, cy] = size(V);
    m = mean(V_X,1);
    M = m(ones(ry,1),:);
    C = V_X - m(ones(rx,1),:);
    [Q,R] = qr(C,0);

    ri = R'\(V-M)';
    
    Y1 = sqrt(sum(ri.*ri,1)');
    Y = NaN(h, w);
    Y(idx_ft) = Y1;
end
