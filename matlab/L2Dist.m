function Y = L2Dist(X, mask)
    
    d = length(X);
    [h, w] = size(X{1});
    X1 = zscore2(X);
    
    for d1=1:d
        V(:, d1) = X1{d1}(:) -  nanmean(X1{d1}(mask));
    end
    
    Y = - sqrt(sum(V.*V, 2));           % 
    Y = reshape(Y, [h, w]);
end
