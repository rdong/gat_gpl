#include <QApplication>
#include <QtPlugin>
#include <QPluginLoader>
#include <QDir>
#include "PluginInterface.h"
#include "loader_diag.h"
#include <iostream>
#include "LogWindow.h"

using namespace qsm;
int main(int argc, char** argv)
{
  QApplication app(argc, argv);
  loader_diag diag;
  QTextStream cerr(stderr, QIODevice::WriteOnly);
  cerr << "Main" << ".\n";
  QDir pluginsDir("plugins");
  
  ImageStack empty;
  LauncherInterface Ilauncher(empty, QList<Shapefile>());

  QList<QPluginLoader*> loaders;
  QList<PluginInterface*> plugins;
  foreach(QString fileName, pluginsDir.entryList(QDir::Files))
    {
      cerr << fileName << ".\n";
      QPluginLoader* loader = new QPluginLoader(pluginsDir.absoluteFilePath(fileName));
      loaders.push_back(loader);
      QObject *plugin = loader->instance();
      if (plugin)
	{
	  LogWindow(diag.log()) << "Found plugin "  << fileName << ".";
	  PluginInterface* sda = qobject_cast<PluginInterface*>(plugin);
	  plugins.push_back(sda);
	  if (sda)
	    {
	      LogWindow(diag.log()) << "whose name is " << sda->name() << ".\n";
	      QWidget* widget_plugin =  sda->launch(&Ilauncher);
	      diag.addTab(widget_plugin, sda->name());
	    }
	}
      else
	LogWindow(diag.log())  << "Failed to load " << loader->errorString() << fileName << ".\n";
    }
  int ret = diag.exec();
  foreach(PluginInterface* plugin, plugins)
    {
      delete plugin;
    }
  
  foreach(QPluginLoader* loader, loaders)
    {
      loader->unload();
    }
  return ret;
}
