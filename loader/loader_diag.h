#ifndef LOADER_DIAG_H
#define LOADER_DIAG_H

#include <QDialog>
#include "ui_loader.h"
#include "LogWindow.h"
#include <QDir>
#include <QtPlugin>
#include <QPluginLoader>
#include "PluginInterface.h"

namespace qsm
{
  class loader_diag: public QDialog, private Ui::loader
  {
    Q_OBJECT
  public:
    loader_diag()
    {
      setupUi(this);
      while (tabWidget->count() > 0) tabWidget->removeTab(0);
    }

    QPlainTextEdit* log() { return logText; }
    
    virtual ~loader_diag() {};

    int addTab(QWidget* page, const QString & label)
    {
      return tabWidget->addTab(page, label);
    }
  };
};
#endif
