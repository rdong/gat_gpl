TEMPLATE = lib
TARGET = gatLauncher
CONFIG += qt
QT += xml 
unix:{
	LIBS += # -lqgis_gui -lqgis_core 
	INCLUDEPATH += /usr/include/qgis
}

INCLUDEPATH += . common /opt/MCR/include

QMAKE_CXXFLAGS_WARN_ON -= -Wall
QMAKE_CXXFLAGS_WARN_ON += -Wall -Wno-unused-parameter -Wno-unused-variable

SOURCES = Launcher.cpp common/Layers2Stack.cpp 
SOURCES	+= common/ImageStack.cpp common/ImageStackMatlab.cpp

HEADERS = Launcher.h common/PluginInterface.h common/Layers2Stack.h common/ImageStack.h

FORMS += Launcher.ui
DEFINES += GUI_EXPORT= CORE_EXPORT=

macx:{
	LIBS += -framework GDAL -framework GEOS -F/Applications/QGIS.app/Contents/Frameworks/ -framework qgis_core -framework qgis_gui
	INCLUDEPATH += /Applications/QGIS.app/Contents/Frameworks/qgis_core.framework/Headers
	INCLUDEPATH += /Library/Frameworks/GEOS.framework/Headers
	INCLUDEPATH += /Library/Frameworks/GDAL.framework/Headers
	INCLUDEPATH += /Applications/QGIS.app/Contents/Frameworks/qgis_core.framework/Headers
	INCLUDEPATH += /Applications/QGIS.app/Contents/Frameworks/qgis_gui.framework/Headers
	LIBS +=	-lazy_library /opt/MCR/bin/arch/libmwmclmcr.dylib
	QMAKE_LIBDIR += /Applications/QGIS.app/Contents/Frameworks/qgis_core.framework/
	QMAKE_EXTENSION_SHLIB = so
}

