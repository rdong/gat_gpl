#include "ImageStack.h"
#include <QFile>
#include <QTextStream>
#include <cmath>
#include <iostream>

using namespace qsm;

template<class T>
Image2D<T>* Image2D<T>::transpose() const
{
  //  std::cerr << "Entering Image2D::transpose.\n";
  Image2D<T>* ret = new Image2D<T>(_cols, _rows);
  T* ret_data = (T*)ret->data();
  const T* this_data = (const T*)_data.data();
  for (size_t r=0; r<_rows; ++r)
    for (size_t c=0;c<_cols; ++c)
      ret_data[c*_rows+r] = this_data[r*_cols+c];

  ret->setName(_name);
  ret->setExtent(_extent);
  return ret;
}

// ToDo:  use nondata value
template<class T>
Image2D<T>* Image2D<T>::zeroPad(size_t l, size_t t, size_t r, size_t b) const
{
  std::cerr << "Entering Image2D<T>::zeroPad.\n";
  const size_t ret_cols = _cols+l+r;
  const size_t ret_rows =  _rows+t+b;
  Image2D<T>* ret = new Image2D<T>(ret_rows, ret_cols);
  
  T* ret_data = (T*)ret->data();
  const T* this_data = (const T*)_data.data();

  QVector<T> z1(ret_cols, 0);
  for (size_t r1=0; r1<t; ++r1)
    memcpy(ret_data+r1*ret_cols, z1.data(), ret_cols*sizeof(T));
	   
  for (size_t r1=t; r1<_rows+t; ++r1)
    {
      memcpy(ret_data+r1*ret_cols, z1.data(), l*sizeof(T));
      const T* this_row = this_data+(r1-t)*_cols;
      T* ret_row = ret_data+r1*ret_cols;
      for (size_t c=0; c< _cols; ++c)
	ret_row[c+l] = std::isnan(this_row[c])?0:this_row[c];
      //memcpy(ret_row, this_row, _cols*sizeof(T));
      memcpy(ret_data+r1*ret_cols+l+_cols, z1.data(), r*sizeof(T));
    }
    
  for (size_t r1=_rows+t; r1<ret_rows; ++r1)
    memcpy(ret_data+r1*ret_cols, z1.data(), ret_cols*sizeof(T));

  ret->setName(_name);
  const float xres = _extent.width()/_cols;
  const float yres = _extent.height()/_rows;
  ret->setExtent(_extent.adjusted(-l*xres, t*yres, r*xres, -b*yres));
  return ret;
}

static int enviType(const quint8&) {return 1; }
static int enviType(const bool&) {return 1; }
//static int enviType(const int16_t&) {return 2; }
//static int enviType(const int32_t&) {return 3; }
static int enviType(const float&) {return 4; }
static int enviType(const double&) {return 5; }
//static int enviType(const uint16_t&) {return 12; }
static int enviType(const quint32&) {return 13; }

template<class T>
bool Image2D<T>::enviWrite(const QString& base) const
{
  QFile hdr_f(base + ".hdr");
  hdr_f.open(QIODevice::WriteOnly);
  QTextStream hdr(&hdr_f);
  
  hdr << "ENVI\nsamples = " << _cols << "\n";
  hdr << "lines = " <<  _rows << "\n";
  hdr << "bands = " << 1 << "\n";
  hdr << "header offset = 0\nfile type = ENVI Standard\n";
  //ToDo: assume all the bands are of the same type
  hdr << "data type = " << enviType((T)0) << "\ninterleave = bsq\nbyte order = 0\n";

  const float xres = _extent.width()/_cols;
  const float yres = _extent.height()/_rows;
  
  hdr << "map info = {UTM, 1, 1, " << _extent.x() << ", " <<  _extent.y() + _extent.height()
      << ", " << xres << ", " << yres << ", 11 , North, WGS-84}\n";
  hdr << "pixel size = { " << xres << ", " << yres << ", units=meters }\n";

  QFile data_f(base);
  data_f.open(QIODevice::WriteOnly); 
  data_f.write((const char*)_data.data(), _rows*_cols*sizeof(T));
  return true;
}

template class Image2D<float>;
template class Image2D<double>;
template class Image2D<quint8>;
template class Image2D<quint32>;
template class Image2D<bool>;
