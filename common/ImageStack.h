#ifndef IMAGESTACK_H
#define IMAGESTACK_H

#include <QString>
#include <QVector>
#include <QList>
#include <QRectF>
#include <QSharedPointer>
class mwArray;

/*
  Transports between QGIS and real plugins.  Use QT only.
*/

namespace qsm
{
  class ImageBase2D
  {
  public:
    static ImageBase2D* fromArray(const mwArray& array);
    ImageBase2D(size_t rows, size_t cols) :_rows(rows), _cols(cols){}
    virtual ~ImageBase2D() {}
    size_t rows() const { return _rows; }
    size_t cols() const { return _cols; }
    virtual mwArray toArray() const = 0;
    virtual void* data() { return 0; }
    virtual size_t dataSize() const { return 0; }
    virtual mwArray toR() const = 0;
    virtual bool enviWrite(const QString& base) const = 0;
    virtual ImageBase2D* zeroPad(size_t l=1, size_t t=1, size_t r=1, size_t b=1) const = 0;

    void setName(QString name) {_name = name; }
    QString name() const { return _name; }
    QRectF extent() const { return _extent; }
    bool setExtent(const mwArray& R);
    void setExtent(const QRectF& extent) { _extent = extent; }
    void setExtent(double x_min, double y_min, double x_max, double y_max)
    {
      _extent.setLeft(x_min);
      _extent.setTop(y_min);
      _extent.setRight(x_max);
      _extent.setBottom(y_max);
    }
    
  protected:
    QString _name;
    size_t _rows;
    size_t _cols;
    QRectF _extent;
    // Used by to Array
    virtual ImageBase2D* transpose() const { return 0;} 
  };

  template<class T>
    class Image2D : public ImageBase2D
  {
  public:
    Image2D(size_t rows, size_t cols) :ImageBase2D(rows, cols), _data(rows*cols){}
    virtual ~Image2D() {}
    void* data() {  return _data.data(); }
    size_t dataSize() const { return sizeof(T); }
    mwArray toArray() const __attribute__((weak));
    mwArray toR() const __attribute__((weak));
    bool enviWrite(const QString& base) const;
    Image2D<T>* zeroPad(size_t l=1, size_t t=1, size_t r=1, size_t b=1) const;
    static ImageBase2D* fromArray(const mwArray& array);

  protected:
    QVector<T> _data;
    // NoCopy
    //    Image2D(const Image2D& rhs) {}
    // No assignment
    //    const Image2D<T>& operator=(const Image2D<T>& rhs) { return *this; }
    Image2D<T>* transpose() const;
  };
  
  class ImageStack
  {
  public:
    ImageStack() {}
    static ImageStack fromArray(const mwArray& array);
    void add(ImageBase2D* image)
    {
      _images.push_back(QSharedPointer<ImageBase2D>(image)); 
    }
    size_t count() const { return _images.count(); }
    ImageBase2D* operator[](size_t i) { return _images[i].data(); }
    virtual ~ImageStack() {}
    mwArray toArray() const;
    mwArray toR() const;
  protected:
    QList<QSharedPointer<ImageBase2D> >  _images;
  };

  // Two classes
  class Shape
  {
  public:
    enum Geometry {Point, Polygon}; // More type
    Shape(Geometry type): _type(type) {}
    virtual ~Shape() {}
    size_t count() const { return _points.count(); }
    const QVector<QPointF>& points() const { return _points; }
    void add(const QPointF& pt) {_points.push_back(pt);}

  protected:
    QVector<QPointF> _points;
    Geometry _type;
    QString _name;
  };
    
  class Shapefile
  {
  public:
    Shapefile() {}
    virtual ~Shapefile() {};
    size_t count() const { return _shapes.count(); }
    void add(const Shape& shape) { _shapes.push_back(shape);}
    void setName(QString name) {_name = name; }
    QString name() const { return _name; }

    mwArray toArray() const;
  protected:
    QList<Shape> _shapes;
    QString _name;
  };

};

#endif

