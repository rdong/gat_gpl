#ifndef PLUGININTERFACE_H
#define PLUGININTERFACE_H

#include "ImageStack.h"
class QWidget;
#include <QObject>

namespace qsm
{
  /* class used for data marshalling between the launcher and plugins */
  class LauncherInterface : public QObject
  {
    Q_OBJECT
  public:
    LauncherInterface(const ImageStack& inImages, const ImageStack& masks,
		    const QList<Shapefile>& inShapes)
      :_inImages(inImages), _masks(masks), _inShapes(inShapes),
       _initialized(false) {}
    virtual ~LauncherInterface() {}
    ImageStack inImages() { return _inImages; }
    ImageStack outImages() { return _outImages; }
    ImageStack masks() { return _masks; }
    QList<Shapefile> inShapes() { return _inShapes; }
    QList<Shapefile> outShapes() { return _outShapes; }

    void setOutputs(const ImageStack& outImages,
		    const QList<Shapefile>& outShapes)
    {
      _outImages = outImages;
      _outShapes = outShapes;
      emit resultsReady();
    }

    bool initialized() const { return _initialized; }
    void setInitialized(bool initialize = true) { _initialized = initialize; }

  signals:
    void resultsReady();
    
  protected:
    ImageStack _inImages;
    ImageStack _masks;
    ImageStack _outImages;
    QList<Shapefile> _inShapes;
    QList<Shapefile> _outShapes;
    bool _initialized;
  };

  class PluginInterface
  {
  public:
    virtual ~PluginInterface() {}
    virtual QString name() = 0;
    virtual QString version() = 0;
    virtual QString description() = 0;
    virtual QWidget* launch(LauncherInterface* iface) = 0;
    virtual void cleanup() = 0;
    virtual void initialize(LauncherInterface* iface) = 0;
  };
};

Q_DECLARE_INTERFACE(qsm::PluginInterface, "qsm.PluginInterface")

#endif
