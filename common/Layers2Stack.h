#ifndef LAYERS2STACK_H
#define LAYERS2STACK_H
class QSize;
class QRectF;
class QgsRasterLayer;
class QgsVectorLayer;
template<class T> class QList;

namespace qsm
{
  class ImageStack;
  class Shapefile;
  ImageStack Layers2Stack(QList<QgsRasterLayer*> rlayers);
  QList<Shapefile> Layers2Shape(const QList<QgsVectorLayer*>& vlayers);
  ImageStack Layers2masks(const QList<QgsVectorLayer*>& vlayers,
			  int w, int h, const QRectF& extent);
};



#endif
