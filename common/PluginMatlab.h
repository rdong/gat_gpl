#ifndef PLUGINMATLAB_H
#define PLUGINMATLAB_H

#include "PluginImpl.h"
#include <mclmcrrt.h>
#include <QApplication>
#include <QVariant>

namespace qsm
{
 class PluginMatlab: public PluginImpl
  {
  public:
    void initialize(LauncherInterface* iface)
    {
      _iface = iface;
      if (qApp->property("qsmLauncher").isValid()) return;
      qApp->setProperty("qsmLauncher", 1);
      std::cerr << "mclInitializeApplication.\n";
      const char* argv[] = {"-nodisplay", "-nojvm", "-xrm", "MATLAB*height:1", "-xrm", "MATLAB*width:1"};
      if (!mclInitializeApplication(argv, 6))
	std::cerr << "mclInitializeApplication failed.\n";
      qAddPostRoutine(PluginMatlab::cleanup1);
    }
    void cleanup(){}

    static void cleanup1()
    {
      if (!qApp->property("qsmLauncher").isValid()) return;
      std::cerr<< "mclTerminateApplication.\n";
      mclTerminateApplication();
    }

  protected:
    LauncherInterface* _iface;
    // For Matlab initialization.  Only once for all plugins.
    static bool _initialized;
 };
};



#endif
