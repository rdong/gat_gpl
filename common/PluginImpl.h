#ifndef PLUGINIMPL_H
#define PLUGINIMPL_H
/*
  Common for all plugins.
 */
#include "PluginInterface.h"

namespace qsm
{
 class PluginImpl: public PluginInterface
  {
  public:
    PluginImpl() :_iface(0){}
    virtual void initialize(LauncherInterface* iface) {};
  protected:
    LauncherInterface* _iface;
 };
};

#endif
