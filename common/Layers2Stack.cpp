#include "Layers2Stack.h"
#include "ImageStack.h" 
#include <limits>
#include <qgis.h>
#include <qgsrectangle.h>
#include <qgsrasterlayer.h>
#include <qgsrasterdataprovider.h>
#include <qgsvectorlayer.h>
#include <qgsvectordataprovider.h>
#include <qgsgeometry.h>
#include <QPainter>

using namespace qsm;
using namespace std;

ImageStack qsm::Layers2Stack(QList<QgsRasterLayer*> rlayers)
{
  ImageStack ret;
  double NINF = numeric_limits<double>::min();
  double INF = numeric_limits<double>::max();
  double rx = 0;
  double ry = 0;
  QgsRectangle min_extent(NINF, NINF, INF, INF);
  foreach(QgsRasterLayer* rlayer1, rlayers)
    {
      QgsRasterDataProvider* rdp=rlayer1->dataProvider();
      if (rdp->bandCount() == 1 && rdp->dataType(1) == QgsRasterDataProvider::Float32)
	{
	  QgsRectangle extent1 = rlayer1->extent();
	  rx = max(rx, (extent1.xMaximum()-extent1.xMinimum())/rlayer1->width());
	  ry = max(ry, (extent1.yMaximum()-extent1.yMinimum())/rlayer1->height());
	  min_extent = min_extent.intersect(&extent1);
	}
    }
  const int w = (int)((min_extent.xMaximum()-min_extent.xMinimum())/rx+0.5);
  const int h = (int)((min_extent.yMaximum()-min_extent.yMinimum())/ry+0.5);

  foreach(QgsRasterLayer* rlayer1, rlayers)
    {
      QgsRasterDataProvider* rdp=rlayer1->dataProvider();
      if (rdp->bandCount() == 1 && rdp->dataType(1) == QgsRasterDataProvider::Float32)
	{
	  Image2D<float>* image1 = new Image2D<float>(h, w);
	  rdp->readBlock(1, min_extent, w, h, image1->data());
	  const double nodata = rdp->noDataValue();
	  if (nodata == nodata)	// not NaN.  ToDo: what about (-)Inf?
	    {
	      std::cerr << rlayer1->name().toStdString() << " has nodata = " << nodata << "\n";
	      float* data = (float*)image1->data();
	      for (int i=0; i<w*h; ++i)
		{
		  if (doubleNear(nodata, data[i])) data[i] = NAN;
		}
	    }
	  image1->setExtent(min_extent.xMinimum(), min_extent.yMinimum(),
			    min_extent.xMaximum(), min_extent.yMaximum());
	  image1->setName(rlayer1->name());
//	  std::cerr << "Image2D: " << image1.name().toStdString() << ", "
//		    << image1->rows() << "x" << image1->cols() << ".\n";
	  ret.add(image1);
	}
    }
  return ret;  
}

QList<Shapefile> qsm::Layers2Shape(const QList<QgsVectorLayer*>& vlayers)
{
  QList<Shapefile> ret;
  foreach(QgsVectorLayer* vlayer1, vlayers)
    {
      Shapefile shapefile1;
      shapefile1.setName(vlayer1->name());
      QgsVectorDataProvider* vdp = vlayer1->dataProvider();
      vdp->select();
      QgsFeature feature;
      while ( vdp->nextFeature(feature) )
	{
	  // polygon
	  QgsPolygon poly = feature.geometry()->asPolygon();
	  Shape shape1(Shape::Polygon);
	  foreach(const QgsPolyline& line, poly)
	    {
	      //	  std::cerr << "Polygon[" << f << "] has " << line.count() << " Points.\n"; 
	      foreach(const QgsPoint& pt, line)
		{
		  shape1.add(QPointF(pt.x(), pt.y()));
		  //std::cerr << "(" << pt.x() << ", " << pt.y() << ")->";
		}
	      //	  std::cerr << ret.ToString() << ".\n";
	    }
	  shapefile1.add(shape1);
	}
      ret.push_back(shapefile1);
    }
  return ret;
}

ImageStack qsm::Layers2masks(const QList<QgsVectorLayer*>& vlayers,
			     int w, int h, const QRectF& extent)
{
  ImageStack ret;
  QgsRenderContext rc;
  rc.setExtent(QgsRectangle(extent.left(), extent.bottom(),
			    extent.right(), extent.top()));
  QgsMapToPixel mtp;
  mtp.setParameters(extent.width()/w, extent.left(),
		    extent.top(), h);
  rc.setMapToPixel(mtp);
		   
  QImage V(w, h, QImage::Format_ARGB32);
  foreach(QgsVectorLayer* vlayer1, vlayers)
    {
      V.fill(QColor(Qt::black).rgb()); //defaults to black
      QPainter myQPainter(&V);
      myQPainter.setPen(Qt::white); // Doens't work
      rc.setPainter(&myQPainter);
      vlayer1->draw(rc);
      //vlayer1->drawRendererV2(rc, false);
      myQPainter.end();
      Image2D<bool>* image1 = new Image2D<bool>(h, w);
      image1->setExtent(extent);
      image1->setName(vlayer1->name());
      const qint32* bits = (const qint32*)V.bits();
      for (int i=0; i<w*h; ++i)
	((bool*)(image1->data()))[i] = (bits[i]&0x00FFFFFF)?1:0;
      ret.add(image1);
    }
  return ret;
}
