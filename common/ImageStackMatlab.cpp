#include "ImageStack.h"
#include "mclcppclass.h"
#include <QFile>
#include <QTextStream>
#include <cmath>

using namespace qsm;

mwArray ImageStack::toArray() const
{
  //  std::cerr << "Entering ImageStack::toArray.\n";
  //ToDo: float only for now
  mwArray ret(count(), 1, mxCELL_CLASS);
  int l = 1;
  foreach(QSharedPointer<ImageBase2D> image1, _images)
    {
      ret(l) = image1->toArray();
      ++l;
    }
  return ret;
}
mwArray ImageStack::toR() const
{
  mwArray ret(count(), 1, mxCELL_CLASS);
  int r=1;
  foreach(QSharedPointer<ImageBase2D> image1, _images)
    {
      ret(r) = image1->toR();
      ++r;
    }
  return ret;
}

ImageStack ImageStack::fromArray(const mwArray& array)
{
  ImageStack ret;
  mxClassID id = array.ClassID();
  if (mxCELL_CLASS != id) return ret;
  if (1 != array.NumberOfDimensions()) return ret;
  for (size_t i=0; i < array.NumberOfElements(); ++i)
    {
      ImageBase2D* image1 = ImageBase2D::fromArray(array.Get(1, i));
      if (0 != image1) ret.add(image1);
    }
  return ret;
}

mwArray Shapefile::toArray() const
{
  const char* fields[] = {"Geometry", "X", "Y"};
  mwArray ret(_shapes.count(), 1, 3, fields);
  int f = 1;
  foreach (Shape shape1, _shapes)
    {
      mwArray X(shape1.count(), 1, mxDOUBLE_CLASS);
      mwArray Y(shape1.count(), 1, mxDOUBLE_CLASS);
      int p = 1;
      foreach (QPointF pt, shape1.points())
	{
	  X(p) = pt.x();
	  Y(p) = pt.y();
	  ++p;
	}
      ret(fields[0], f).Set("Polygon");
      ret(fields[1], f).Set(X);
      ret(fields[2], f).Set(Y);
      ++f;
    }
  return ret;
}


template<class T>
mwArray Image2D<T>::toR() const
{
  mwArray R(3, 2, mxDOUBLE_CLASS);
  R(1, 1) = 0;
  R(1, 2) = -(extent().height())/rows();
  R(2, 2) = 0;
  R(2, 1) = (extent().width())/cols();
  R(3, 1) = extent().x();
  R(3, 2) = extent().y()+extent().height();
  return R;
}

static mxClassID classID(const uint8_t&) {return mxUINT8_CLASS; }
//static mxClassID classID(const bool&) {return mxLOGICAL_CLASS; }
//static mxClassID classID(const uint16_t&) {return mxINT16_CLASS; }
static mxClassID classID(const uint32_t&) {return mxUINT32_CLASS; }
static mxClassID classID(const float&) {return mxSINGLE_CLASS; }
static mxClassID classID(const double&) {return mxDOUBLE_CLASS; }

template<class T>
mwArray Image2D<T>::toArray() const
{
  //  std::cerr << "Entering Image2D<T>::toArray.\n";
  Image2D<T>* image2 = transpose();
  T ignore;
  mwArray m = mwArray(image2->cols(), image2->rows(), classID(ignore), mxREAL);

  m.SetData((T*)image2->data(), image2->rows()*image2->cols());
  delete image2;
  return m;
}

namespace qsm
{
  template<>
  mwArray Image2D<bool>::toArray() const
  {
    //  std::cerr << "Entering Image2D<T>::toArray.\n";
    Image2D<bool>* image2 = transpose();
    bool ignore;
    mwArray m = mwArray(image2->cols(), image2->rows(), mxLOGICAL_CLASS, mxREAL);

    m.SetLogicalData((bool*)image2->data(), image2->rows()*image2->cols());
    delete image2;
    return m;
  }

  template<>
  ImageBase2D* Image2D<bool>::fromArray(const mwArray& array)
  {
    //  std::cerr << "entering Image2D<bool>::fromArray.\n";
    mwArray dims = array.GetDimensions();
    //  std::cerr << "GetDimensions() = " << dims << ".\n";
    uint32_t rows = dims(1);
    uint32_t cols = dims(2);
    std::cerr << "fromArray (" << rows << ", " << cols << ").\n";
    Image2D<bool>* ret1 = new Image2D<bool>(cols, rows);
    array.GetLogicalData((bool*)ret1->data(), rows*cols);
    ImageBase2D* ret = ret1->transpose();
    delete ret1;
    return ret;
  }
}

bool ImageBase2D::setExtent(const mwArray& R)
{
  if (!R.IsNumeric()) return false;
  if (mxDOUBLE_CLASS != R.ClassID()) return false;
  if (2 != R.NumberOfDimensions()) return false;
  mwArray dims = R.GetDimensions();
  uint32_t dim_rows = dims(1);
  uint32_t dim_cols = dims(2);
  if (3 != dim_rows || 2 != dim_cols) return false;
  const double left = R(3, 1);
  const double bottom = R(3, 2);
  const double xres = R(2, 1);
  const double yres_neg = R(1, 2);
  setExtent(left, bottom, left+xres*_cols, bottom-_rows*yres_neg);
  return true;
}

ImageBase2D* ImageBase2D::fromArray(const mwArray& array)
{
  std::cerr << "entering ImageBase2D::fromArray.\n";
  if (!array.IsNumeric()) return 0;
  if (2 != array.NumberOfDimensions()) return 0;
  switch (array.ClassID())
    {
    case mxDOUBLE_CLASS:
      return Image2D<double>::fromArray(array);
      break;
    case mxSINGLE_CLASS:
      return Image2D<float>::fromArray(array);
      break;
    case mxUINT8_CLASS:
      return Image2D<uint8_t>::fromArray(array);
      break;
    case mxUINT32_CLASS:
      return Image2D<uint32_t>::fromArray(array);
      break;
    case mxLOGICAL_CLASS:
      return Image2D<bool>::fromArray(array);
      break;
    default:
      return 0;
    }
  return 0;
}

// already check the dim etc.
template<class T>		
ImageBase2D* Image2D<T>::fromArray(const mwArray& array)
{
  //  std::cerr << "entering Image2D<T>::fromArray.\n";
  mwArray dims = array.GetDimensions();
  //  std::cerr << "GetDimensions() = " << dims << ".\n";
  uint32_t rows = dims(1);
  uint32_t cols = dims(2);
  std::cerr << "fromArray (" << rows << ", " << cols << ").\n";
  Image2D<T>* ret1 = new Image2D<T>(cols, rows);
  array.GetData((T*)ret1->data(), rows*cols);
  ImageBase2D* ret = ret1->transpose();
  delete ret1;
  return ret;
}

