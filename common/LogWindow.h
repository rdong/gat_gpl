#ifndef LOGWINDOW_H
#define LOGWINDOW_H
#include <QPlainTextEdit>
#include <QTextStream>

namespace qsm
{
  class LogWindow
  {
  public:
    LogWindow(QPlainTextEdit* parent): _parent(parent) {}
    void clear() { _parent->clear(); }
    template<class T> 
    LogWindow& operator<<(const T& rhs)
    {
      QTextStream(&_log) << rhs;
      return *this;
    }
      
    virtual ~LogWindow() { _parent->appendPlainText(_log); }

  protected:
    QString _log;
    QPlainTextEdit* _parent;
  };
};

#endif
