#include "Layers2Stack.h"
#include "ImageStack.h"
#include "PluginInterface.h"
#include "LogWindow.h"

#ifdef WIN32
#define QGISEXTERN extern "C" __declspec( dllexport )
#else
#define QGISEXTERN extern "C"
#endif

#include <qgisinterface.h>
#include <qgsmapcanvas.h>
#include <qgsmaplayer.h>
#include <qgsrasterlayer.h>
#include <qgsvectorlayer.h>
#include <qgsvectordataprovider.h>
#include <QAction>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>

#include "Launcher.h"
#include <iostream>

QGISEXTERN QgisPlugin* classFactory(QgisInterface *iface)
{
  std::cerr << "classFactory Launcher.\n";
  QProcessEnvironment env = QProcessEnvironment::systemEnvironment();

  //QFile file("dev/ttys004");
  //if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) return 0;

  //QTextStream out(&file);
  std::ostream& out = std::cerr;

  foreach (QString str, env.toStringList())
    {
      out << str.toStdString() << ".\n";;
    }

  return new qsm::Launcher(iface);
}

QGISEXTERN QString name() { std::cerr << "name = GAT\n";  return "GAT"; }

QGISEXTERN QString description()
{
  std::cerr << "description = Geospatial Analytics Tools.\n";
  return "Geospatial Analytics Tools";
}

QGISEXTERN QString version() { return "0.3"; }

QGISEXTERN int type() { return QgisPlugin::UI;}

// ToDo: Not getting called?
QGISEXTERN void unload()
{
  std::cerr << "Launcher unload().\n";
}

using namespace qsm;

void Launcher::initGui()
{
  _LaunchAction = new QAction(tr("Launch"), this);
  connect(_LaunchAction, SIGNAL(activated()), this, SLOT(launch()));
  //_Iface->addToolBarIcon(_LaunchAction);
  _Iface->addPluginToMenu(tr("&gat"), _LaunchAction);
}

void Launcher::launch()
{
  std::cerr << "Entering Launcher::Launch.\n";
  QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
  foreach (QString str, env.toStringList())
    {
      std::cerr << str.toStdString() << ".\n";;
    }
    
  QList<QgsMapLayer*> layers = _Iface->mapCanvas()->layers();
  QList<QgsRasterLayer*> rlayers;
  QList<QgsVectorLayer*> vlayers;
  foreach(QgsMapLayer* layer1, layers)
    {
      switch (layer1->type())
	{
	case QgsMapLayer::RasterLayer:
	  {
	    QgsRasterLayer* rlayer1 = (QgsRasterLayer*)layer1;
	    QgsRasterDataProvider* rdp=rlayer1->dataProvider();
	    if (rdp->bandCount() == 1 && rdp->dataType(1) == QgsRasterDataProvider::Float32)
	      rlayers.push_back(rlayer1);
	    break;
	  }
	case QgsMapLayer::VectorLayer:
	  {
	    QgsVectorLayer* vlayer1 = (QgsVectorLayer*)layer1;
	    QgsVectorDataProvider* vdp = vlayer1->dataProvider();
	    if ( vdp->geometryType() == QGis::WKBPolygon )
	      vlayers.push_back(vlayer1);
	    break;
	  }
	default:
	  break;
	}
    }
  ImageStack stack = Layers2Stack(rlayers);
  //if (stack.count() > 0) stack[0]->enviWrite("stack0");
  
  QList<Shapefile> shapefiles = Layers2Shape(vlayers);
  ImageStack masks; 
  if (stack.count() > 0)
    {
      masks = Layers2masks(vlayers, stack[0]->cols(),
				      stack[0]->rows(), stack[0]->extent());

      //if (masks.count() > 0) masks[0]->enviWrite("masks0");
    }

  LauncherInterface Ilaunch(stack, masks, shapefiles);

  Launcher_diag diag(&Ilaunch, _Iface);
  diag.exec();
}

void Launcher::unload()
{
  //_Iface->removeToolBarIcon(_LaunchAction);
  _Iface->removePluginMenu(tr("&Launch"), _LaunchAction);
  delete _LaunchAction;
  _LaunchAction = 0;

  qWarning("Launcher::unload.\n");
}

using namespace qsm;
Launcher_diag::Launcher_diag(LauncherInterface* Iface, QgisInterface* qgis)
  :_Iface(Iface), _qgis(qgis)
{
  setupUi(this);
  while (tabWidget->count() > 0) tabWidget->removeTab(0);
  connect(Iface, SIGNAL(resultsReady()), this, SLOT(resultsReady()));

  QSettings settings;
  QString myPaths = settings.value( "plugins/searchPathsForPlugins", "" ).toString();
  std::cerr << myPaths.toStdString() << ".\n";

  if ( myPaths.isEmpty() ) return;


  QStringList myPathList = myPaths.split( "|" );
  foreach(QString path, myPathList)
    {
      QDir pluginsDir(path+"/plugins");
      foreach(QString fileName, pluginsDir.entryList(QDir::Files))
	{
	  QPluginLoader* loader = new QPluginLoader(pluginsDir.absoluteFilePath(fileName));
	  QObject *plugin = loader->instance();
	  if (plugin)
	    {
	      LogWindow(log()) << "Found plugin "  << fileName << ".";
	      PluginInterface* sda = qobject_cast<qsm::PluginInterface*>(plugin);
	      if (sda)
		{
		  QWidget* widget_plugin =  sda->launch(Iface);
		  addTab(widget_plugin, sda->name());
		  _loaders.push_back(loader);
		}
	    }
	  else
	    LogWindow(log()) << "Failed to load " << loader->errorString() << fileName << ".";
	}
    }
}

Launcher_diag::~Launcher_diag()
{
  foreach (QPluginLoader* loader, _loaders)
    {
      //      loader->unload();
    }
}

void Launcher_diag::resultsReady()
{
  std::cerr << "Launcher_diag::resultsReady.\n";

  for (size_t i=0; i < _Iface->outImages().count(); ++i)
    {
      ImageBase2D* image1 = _Iface->outImages()[i];

      QgsRasterLayer* cur_layer = _qgis->addRasterLayer(image1->name(),
							image1->name());
      cur_layer->setStandardDeviations(3.0);
      cur_layer->setColorShadingAlgorithm(QgsRasterLayer::PseudoColorShader);
      cur_layer->setDrawingStyle(QgsRasterLayer::SingleBandPseudoColor);
      cur_layer->triggerRepaint();
    }

  for (int i=0; i < _Iface->outShapes().count(); ++i)
    {
      Shapefile shp1 = _Iface->outShapes()[i];

      QgsVectorLayer* cur_layer = _qgis->addVectorLayer(shp1.name(),
							shp1.name(), "ogr");
      bool ret;
      QString status = cur_layer->loadNamedStyle("jet.qml", ret);
      cur_layer->triggerRepaint();
      //      std::cerr << status.toStdString() << ".\n";;
    }


}
